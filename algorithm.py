import os
import time

import numpy as np
from numpy._typing import NDArray
from PIL import Image

from picture import Picture

def run(image_path: str, population: int, mutate_rate: float) -> None:
    """
    The runner for this program. Takes in command-line arguments and continuously loops.
    """

    source_image = Image.open(image_path)
    source_image_pixels = np.asarray(source_image)

    num_triangles = int(np.cbrt(source_image.width * source_image.height) * 10)
    generation = 0

    # Create and initialize the first generation
    pictures = np.array([Picture(source_image.size, num_triangles) for _ in range(population)], dtype=Picture)
    difference = 1

    print(f"""
{'-' * 30}
Start time: {time.strftime('%H:%M:%S')}
Image: {image_path}
Dimensions: {source_image.width}x{source_image.height}
Triangles: {num_triangles}
{'-' * 30}
""")

    while difference != 0:
        successor, difference = select_successor(pictures, source_image_pixels)
        pictures = create_next_generation(pictures, successor, mutate_rate)

        if generation % 100 == 0:
            print(f"[{time.strftime('%H:%M:%S')}] Difference after {generation} generations: {difference}")
            successor.content.save(os.path.join("out/", f"{generation}.jpg"))
    
        generation += 1


def select_successor(pictures: NDArray, source_image_pixels: NDArray[np.uint8]) -> tuple[Picture, int]:
    """
    The image in the current generation with the least difference compared to the source image is
    selected to be the successor image used in the next generation.
    """

    min_difference = float("inf")
    min_index = 0

    for i, picture in enumerate(pictures):
        picture.draw_triangles()

        picture_pixels = np.asarray(picture.content)
        difference = np.sum(np.abs(source_image_pixels - picture_pixels))

        if difference < min_difference:
            min_difference = difference
            min_index = i

    return (pictures[min_index], min_difference)


def create_next_generation(pictures: NDArray, successor: Picture, mutate_rate: float) -> NDArray:
    """
    Creates the next generation using the "best" image of the previous generation as a base. Images
    have their triangles mutated based on the provided mutation rate.
    """

    for picture in pictures:
        picture.triangle_colors = successor.triangle_colors.copy()
        picture.triangle_edges = successor.triangle_edges.copy()

        picture.mutate_triangles(mutate_rate)

    return pictures
