# Image replicator

## Requirements

- Python 3.9 or above is recommended
- PIP Packages:
  - Pillow - https://pypi.org/project/Pillow/
  - NumPy - https://numpy.org/

## About

This program recreates images through the use of an asexual reproduction algorithm. A image is 
taken in as input and stored to be used for rating the **fitness** of a generated image (i.e., how 
similar the image is to the original). Generated images contain several colored triangles with RGBA
values.

## Execution

```
$ ./main.py -h
Usage:
    ./main.py [PATH TO IMAGE] [IMAGES PER GENERATION] [MUTATION RATE]

Arguments:
    PATH TO IMAGE:          A relative path for an existing image to recreate
    IMAGES PER GENERATION:  The number of images produced per generation. Must be greater than 0.
    MUTATION RATE:          The percentage a triangle will mutate between generations. Values must be between 0 and 100.
```
